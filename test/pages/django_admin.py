from django_selenium_clean import selenium, SeleniumTestCase, PageElement
from selenium.webdriver.common.by import By


class DjangoAdminSection:
    # header_admin = PageElement(By.LINK_TEXT, 'Admin')
    server_url = "localhost:8000"

    def __init__(self, server_url):
        super().__init__()
        self.server_url = server_url

    def is_recognized(self):
        title = selenium.driver.title
        # return self.header_admin.is_displayed();
        return "Django site admin" in title

    def navigate_django_admin(self):
        selenium.driver.get(self.server_url + "/admin")
        self.login()

    def login(self):
        login_form = PageElement(By.ID, "login-form")
        login_form.wait_until_is_displayed()
        input_username = PageElement(By.ID, "id_username")
        input_password = PageElement(By.ID, "id_password")
        button_login = PageElement(By.CSS_SELECTOR, "input[type='submit'")
        input_username.wait_until_is_displayed()
        input_password.wait_until_is_displayed()
        button_login.wait_until_is_displayed()
        if input_username.exists() and input_password.exists() and button_login.exists():
            input_username.send_keys("crypt")
            input_password.send_keys("hjkkjh128")
            button_login.click()
        else:
            raise Exception("Login impossibile")

    def create_object(self, type, object):
        selenium.driver.get(self.server_url + "/admin/Gallery" + type)
