from unittest import skipUnless

from django_selenium_clean import selenium, SeleniumTestCase, PageElement
from selenium.webdriver.common.by import By


@skipUnless(selenium, "Selenium is unconfigured")
class HelloTestCase(SeleniumTestCase):
    heading_breadcrumbs = PageElement(By.ID, 'breadcrumbs')
    heading_facebook = PageElement(By.ID, 'facebook')
    content_albums = PageElement(By.ID, 'albums')

    def test_index_page_available(self):
        selenium.get(self.live_server_url)
        self.heading_breadcrumbs.wait_until_is_displayed()
        self.assertTrue(self.heading_breadcrumbs.is_displayed())
        self.heading_breadcrumbs.wait_until_contains("Gallery")
        pass

    def test_facebook_available(self):
        selenium.get(self.live_server_url)
        self.assertTrue(self.heading_facebook.exists())
        pass

    def test_index_albums_available(self):
        selenium.get(self.live_server_url)
        self.content_albums.wait_until_is_displayed()
        self.assertTrue(self.content_albums.is_displayed())
        pass
