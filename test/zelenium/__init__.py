from django_selenium_clean import PageElement, selenium


class PageElements(PageElement):
    def __init__(self, *args):
        super().__init__(*args)

    def __getitem__(self, item):
        items = selenium.find_elements(*self.locator)
        if item < len(items):
            return items[item]
        else:
            return None

    def __len__(self):
        return len(selenium.find_elements(*self.locator))
