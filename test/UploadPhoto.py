from datetime import date
from unittest import skipUnless

from django_selenium_clean import selenium, SeleniumTestCase, PageElement
from selenium.webdriver.common.by import By
from Gallery.models import *
from test.zelenium import PageElements


@skipUnless(selenium, "Selenium is unconfigured")
class UploadPhoto(SeleniumTestCase):
    container_albums = PageElement(By.ID, 'albums')
    albums = PageElement(By.CLASS_NAME, "album")

    # def test_create_album(self):
    #    selenium.get(self.live_server_url)

    def test_index_albums_available(self):
        selenium.get(self.live_server_url)
        self.container_albums.wait_until_is_displayed()
        self.assertTrue(self.container_albums.is_displayed())
        pass

    # def test_open_album(self):
    #     selenium.get(self.live_server_url)
    #     self.container_albums.wait_until_is_displayed()
    #     self.albums.wait_until_exists()
    #     self.assertGreater(len(self.albums), 0)

    def test_create_user_album_participant(self):
        from django.db import models
        u = User(first_name='TestFrancesco',
                 last_name='TestFiduccia',
                 nick_name='Test')
        u.save()
        a = Album(name='TestAlbum', location='Test',
                  start_date=date.today(),
                  end_date=date.today(),
                  upload_element_limit=10,
                  upload_size_limit=100
                  )
        a.save()

        p = Participant(album=a, user=u)
        p.save()
        selenium.get(self.live_server_url + "/album/1")
        breadcrumbs = PageElements(By.CLASS_NAME, "breadcrumb")
        self.assertTrue(breadcrumbs.exists())
        self.assertTrue(len(breadcrumbs) == 2)
        button_upload = PageElement(By.LINK_TEXT, "Upload")
        self.assertTrue(button_upload.exists())
        button_upload.click()
