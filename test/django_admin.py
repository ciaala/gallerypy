from unittest import skipUnless

from django_selenium_clean import selenium, SeleniumTestCase, PageElement
from selenium.webdriver.common.by import By

from test.pages.django_admin import DjangoAdminSection


@skipUnless(selenium, "Selenium is unconfigured")
class TestAccessDjangoAdmin(SeleniumTestCase):
    def __init__(self, methodName='runTest'):
        super().__init__(methodName)

    def testNavigationDjangoAdmin(self):
        adminSection = DjangoAdminSection(self.live_server_url)
        adminSection.navigate_django_admin()
        self.assertTrue(adminSection.is_recognized(), "Unable to identify the Admin Page")
