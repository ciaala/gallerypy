import json
import logging
from django.utils import six

logger = logging.getLogger(__name__)

try:
    unicode = unicode
except NameError:
    unicode = str


def parse_signed_data(signed_request, secret):
    '''
    Thanks to
    http://stackoverflow.com/questions/3302946/how-to-base64-url-decode-in-python
    and
    http://sunilarora.org/parsing-signedrequest-parameter-in-python-bas
    '''

    l = signed_request.split('.', 2)
    encoded_sig = l[0]
    payload = l[1]

    sig = base64_url_decode_php_style(encoded_sig)
    import hmac
    import hashlib
    data = json.loads(base64_url_decode_php_style(payload).decode('utf-8'))

    algo = data.get('algorithm').upper()
    if algo != 'HMAC-SHA256':
        error_format = 'Unknown algorithm we only support HMAC-SHA256 user asked for %s'
        error_message = error_format % algo
        send_warning(error_message)
        logger.error('Unknown algorithm')
        return None
    else:
        expected_sig = hmac.new(smart_str(secret), msg=smart_str(payload),
                                digestmod=hashlib.sha256).digest()

    if not hmac.compare_digest(sig, expected_sig):
        error_format = 'Signature %s didnt match the expected signature %s'
        error_message = error_format % (sig, expected_sig)
        send_warning(error_message)
        return None
    else:
        logger.debug('valid signed request received..')
        return data


def base64_url_decode_php_style(inp):
    '''
    PHP follows a slightly different protocol for base64 url decode.
    For a full explanation see:
    http://stackoverflow.com/questions/3302946/how-to-base64-url-decode-in-python
    and
    http://sunilarora.org/parsing-signedrequest-parameter-in-python-bas
    '''
    import base64
    padding_factor = (4 - len(inp) % 4) % 4
    inp += "=" * padding_factor
    return base64.b64decode(unicode(inp).translate(
        dict(zip(map(ord, u'-_'), u'+/'))))


def smart_str(s, encoding='utf-8', strings_only=False, errors='strict'):
    """
    Adapted from django, needed for urlencoding
    Returns a bytestring version of 's', encoded as specified in 'encoding'.
    If strings_only is True, don't convert (some) non-string-like objects.
    """
    import types
    if strings_only and isinstance(s, (types.NoneType, int)):
        return s
    elif not isinstance(s, six.string_types):
        try:
            return str(s)
        except UnicodeEncodeError:
            if isinstance(s, Exception):
                # An Exception subclass containing non-ASCII data that doesn't
                # know how to print itself properly. We shouldn't raise a
                # further exception.
                return ' '.join([smart_str(arg, encoding, strings_only,
                                           errors) for arg in s])
            return unicode(s).encode(encoding, errors)
    elif isinstance(s, unicode):
        return s.encode(encoding, errors)
    elif s and encoding != 'utf-8':
        return s.decode('utf-8', errors).encode(encoding, errors)
    else:
        return s


def send_warning(message, request=None, e=None, **extra_data):
    '''
    Uses the logging system to send a message to logging and sentry
    '''
    username = None
    if request and request.user.is_authenticated():
        username = request.user.username

    error_message = None
    if e:
        error_message = unicode(e)

    data = {
        'username': username,
        'body': error_message,
    }
    data.update(extra_data)
    logger.warn(message,
                exc_info=sys.exc_info(), extra={
            'request': request,
            'data': data
        })
