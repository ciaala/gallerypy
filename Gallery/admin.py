from django.contrib import admin
from Gallery.models import *

# Register your models here.

admin.site.register(User)
admin.site.register(Photo)
admin.site.register(Album)
admin.site.register(Participant)
