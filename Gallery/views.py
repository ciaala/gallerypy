from datetime import datetime
from django.shortcuts import render

from Gallery.facebook import config
from Gallery.models import Album
import logging

# Create your views here.
from django.http import HttpResponse

logger = logging.getLogger(__name__)


def index(request):
    # return HttpResponse("Hello, world. You're at the Gallery index.")
    context = {
        'albums': Album.objects.all(),
        'breadcrumb': [
            {'name': 'Gallery', 'link': "/"},
        ]
    }
    return render(request, 'Gallery/index.html', context)


def album(request, album_id):
    a = Album.objects.get(id=album_id)
    if config.API_KEY in request.COOKIES:
        cookie_value = request.COOKIES[config.API_KEY]
        # request.get_signed_cookie()
        from Gallery.facebook import parse_signed_data
        data = parse_signed_data(cookie_value, config.SALT)
        formatted_date = datetime.fromtimestamp(data['issued_at']).strftime('%Y-%m-%d %H:%M:%S')
        logger.error("Login executed on date %s", formatted_date)
    context = {
        'album': a,
        'breadcrumb': [
            {'name': 'Gallery', 'link': "/"},
            {'name': a.name, 'link': "/album/" + str(a.id) + "/"},
        ]
    }
    return render(request, 'Gallery/album.html', context)
