from django.db import models

# Create your models here.

ENTITY_STATE = (
    ('delete', 'Deleted'),
    ('active', 'Active'),
    ('inactive', 'Inactive'),
)


class User(models.Model):
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    nick_name = models.CharField(max_length=64)
    external_id = models.CharField(max_length=64)
    state = models.CharField(max_length=16, choices=ENTITY_STATE, default='active')

    def __str__(self):
        if self.nick_name:
            return "'%s'" % (self.nick_name)
        else:
            return "%s %s" % (self.first_name, self.last_name)


class Album(models.Model):
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=512)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    location = models.CharField(max_length=64)
    upload_element_limit = models.IntegerField(null=True)
    upload_size_limit = models.IntegerField(null=True)
    state = models.CharField(max_length=16, choices=ENTITY_STATE, default='active')
    members = models.ManyToManyField(User, through='Participant')

    def __str__(self):
        return self.name


class Participant(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    state = models.CharField(max_length=16, choices=ENTITY_STATE, default='active')

    def __str__(self):
        if self.state == 'deleted':
            return "%s %s [deleted]" % (self.user, self.album)
        else:
            return "%s %s" % (self.user, self.album)

    def photos(self):
        return Photo.objects.filter(owner=self)


class Photo(models.Model):
    owner = models.ForeignKey(Participant, on_delete=models.CASCADE, related_name='owned_photos')
    uri = models.CharField(max_length=4096)
    name = models.CharField(max_length=128)
    tag = models.CharField(max_length=128)
    upload_date = models.DateField()
    description = models.CharField(max_length=4096)
    state = models.CharField(max_length=16, choices=ENTITY_STATE, default='active')
    faces = models.ManyToManyField(Participant, verbose_name="list of faces")

    def __str__(self):
        if self.state == 'deleted':
            return "%s: %s[deleted]" % (self.owner, self.name)
        else:
            return "%s: %s" % (self.owner, self.name)
