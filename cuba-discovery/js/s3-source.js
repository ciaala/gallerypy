var CONFIG = {

    BUCKET: "cuba-9754",
};

if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function (suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
}
clearUserGallery = function () {
    var fotorama = $('#fotorama').data('fotorama');
    /*var entries = [ ];
     fotorama.data = [];
     fotorama.size = 0;
     fotorama.load(entries);*/
    fotorama.destroy();

};
setupUserGallery = function (user) {
    $.get('user/' + user + '/img.lst', function (data) {
        var fotorama = $('#fotorama').data('fotorama');
        var lines = data.split('\n');
        var entries = [];
        var i = 0;
        for (i; i <= lines.length; i++) {
            var line = lines[i];
            if (line !== undefined && line !== null && ( line.endsWith('jpg') || line.endsWith('MP4') || line.endsWith('jpeg') || line.endsWith('JPG') || line.endsWith('mp4'))) {
                var prefix = 'https://s3-eu-west-1.amazonaws.com/'
                var image = prefix + CONFIG.BUCKET + '-resized/images/' + line;
                var thumb = prefix + CONFIG.BUCKET + '-thumbnail/images/' + line;
                var full = prefix + CONFIG.BUCKET + '/images/' + line;
                var download = '/download/' + CONFIG.BUCKET + '/images/' + line;
                var entry = {
                    'img': image,
                    'thumb': thumb,
                    'full': full,
                    'download': download,
                    'html': "<a class='overlay' onclick='downloadImage();'>Download Image</a>",
                    'thumbratio': 1
                };
                entries.push(entry);
            }
        }
        //console.log(entries);
        fotorama.load(entries);
    }, 'text');
};

downloadImage = function () {
    var fotorama = $('#fotorama').data('fotorama');
    var frame = fotorama.activeFrame;
    console.log(frame.download);


    var tokens = frame.full.split('/');
    var name = tokens[tokens.length - 1];

    var link = document.createElement('a');
    link.style = 'position: fixed;display:none !important';
    link.href = frame.download;
    link.download = name;
    document.body.appendChild(link);
    link.click();
    setTimeout(function () {
        document.body.removeChild(link)
    }, 100);
    ;

};
setTimeout(
    function () {
        var i;
        var options = $('#user').children();
        for (i = 0; i < options.length; i++) {
            var option = options[i];
            if (option.value && option.value.length > 0) {
                (function (opt) {
                    $.get('user/' + opt.value + '/img.lst', function (data) {
                        var count = 0;
                        var lines = data.split('\n');
                        var i = 0;
                        for (i = 0; i <= lines.length; i++) {
                            var line = lines[i];
                            if (line !== undefined && line !== null && ( line.endsWith('jpg') || line.endsWith('MP4') || line.endsWith('jpeg') || line.endsWith('JPG') || line.endsWith('mp4'))) {
                                count++;
                            }
                        }
                        opt.text = opt.text + " [" + count + "]";
                    });
                })(option);
            }
        }
    },
    3);